<?php
/**
 * The front page template file
 *
 * If the user has selected a static page for their homepage, this is what will
 * appear.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>
    <header id="masthead" class="site-header" role="banner">
        
        <?php get_template_part( 'template-parts/header/header', 'image' ); ?>
        
        <?php if(has_nav_menu( 'top' )) : ?>
            <div class="navigation-top">
                <div class="wrap">
                    <?php get_template_part( 'template-parts/navigation/navigation', 'top' ); ?>
                </div><!-- .wrap -->
            </div><!-- .navigation-top -->
        <?php endif; ?>
    </header><!-- #masthead -->

<?php

/*
 * If a regular post or page, and not the front page, show the featured image.
 * Using get_queried_object_id() here since the $post global may not be set before a call to the_post().
 */
if(( is_single() || ( is_page() && !twentyseventeen_is_frontpage() ) ) && has_post_thumbnail( get_queried_object_id() )) :
    echo '<div class="single-featured-image-header">';
    echo get_the_post_thumbnail( get_queried_object_id(), 'twentyseventeen-featured-image' );
    echo '</div><!-- .single-featured-image-header -->';
endif;
?>

<div class="site-content-contain">
    <div id="content" class="site-content">
        <div id="masthead" class="site-header">
            <div class="wrap">
                <div class="header-blocks">
                    <div class="header-block">
                        <div class="header-block-content">
                            <div class="header-block-image">
        <!--                        <img src="http://localhost:8001/wp-content/uploads/2018/01/strechy-1.svg">-->
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 141.66 89.97">
                                    <defs>
                                        <style>.cls-1 {
                                                fill: #fff;
                                            }</style>
                                    </defs>
                                    <title>strechy</title>
                                    <g id="strechy" data-name="Vrstva 2">
                                        <g id="strechy">
                                            <rect class="cls-1" x="29.07" y="0.7" width="35.91" height="10.42"/>
                                            <rect class="cls-1" x="29.07" y="0.7" width="10.99" height="54.98"/>
                                            <rect class="cls-1" x="14.53" y="48.91" width="10.99" height="44.21" transform="translate(58.96 8.34) rotate(47.6)"/>
                                            <rect class="cls-1" x="33.13" y="45.23" width="6.93" height="14.94"/>
                                            <path class="cls-1" d="M65,.74a0,0,0,0,0,0,0h-9.7a0,0,0,0,0,0,0l0,36.11c3.49-3,7-6,9.75-8.38Z"/>
                                            <rect class="cls-1" x="67.38" y="-2.42" width="9.78" height="36.34" transform="translate(37.04 -49.29) rotate(49.27)"/>
                                            <rect class="cls-1" x="102.64" y="-7.32" width="10.99" height="90" transform="translate(1.6 79.69) rotate(-40.73)"/>
                                        </g>
                                    </g>
                                </svg>
                            </div>
                            <p>Střechy</p>
                            <ul class="submenu-services">
                                <li><a href="#">klempířské práce</a></li>
                                <li><a href="#">pokrývačské práce</a></li>
                                <li><a href="#">tesařské práce</a></li>
                                <li><a href="#">rekonstrukce střech</a></li>
                            </ul>
                        </div>
        
                    </div>
                    <div class="header-block">
                        <div class="header-block-content">
                            <div class="header-block-image">
        <!--                        <img src="http://localhost:8001/wp-content/uploads/2018/01/drevostavby.svg">-->
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 146.94 98.8">
                                    <defs>
                                        <style>.cls-1 {
                                                fill: #fff;
                                            }</style>
                                    </defs>
                                    <title>drevostavby</title>
                                    <g id="drevostavby" data-name="Vrstva 2">
                                        <g id="drevostavby">
                                            <rect class="cls-1" x="105.05" y="-9.87" width="12.43" height="106.28" rx="5.5" ry="5.5" transform="translate(-2.79 78.53) rotate(-38.43)"/>
                                            <rect class="cls-1" x="77.61" y="17.56" width="12.43" height="43.18" rx="5.5" ry="5.5" transform="translate(122.97 -44.67) rotate(90)"/>
                                            <rect class="cls-1" x="75.61" y="17.63" width="12.43" height="74.32" rx="5.5" ry="5.5" transform="translate(136.62 -27.04) rotate(90)"/>
                                            <rect class="cls-1" x="73.98" y="19.22" width="12.43" height="102.43" rx="5.5" ry="5.5" transform="translate(150.64 -9.76) rotate(90)"/>
                                            <rect class="cls-1" x="42.8" y="-16.54" width="12.43" height="132.64" rx="5.5" ry="5.5" transform="translate(49.56 -20.08) rotate(45)"/>
                                        </g>
                                    </g>
                                </svg>
                            </div>
                            <p>Dřevostavby</p>
                        </div>
                    </div>
                    <div class="header-block">
                        <div class="header-block-content">
                            <div class="header-block-image">
        <!--                        <img src="http://localhost:8001/wp-content/uploads/2018/01/stresni_okna.svg">-->
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 141.66 89.97">
                                    <defs>
                                        <style>.cls-1 {
                                                fill: #fff;
                                            }</style>
                                    </defs>
                                    <title>stresni_okna</title>
                                    <g id="stresni_okna" data-name="Vrstva 2">
                                        <g id="stresni_okna">
                                            <rect class="cls-1" x="29.07" y="0.7" width="35.91" height="10.42"/>
                                            <rect class="cls-1" x="29.07" y="0.7" width="10.99" height="54.98"/>
                                            <rect class="cls-1" x="14.53" y="48.91" width="10.99" height="44.21" transform="translate(58.96 8.34) rotate(47.6)"/>
                                            <rect class="cls-1" x="33.13" y="45.23" width="6.93" height="14.94"/>
                                            <path class="cls-1" d="M65,.74a0,0,0,0,0,0,0h-9.7a0,0,0,0,0,0,0l0,36.11c3.49-3,7-6,9.75-8.38Z"/>
                                            <rect class="cls-1" x="67.38" y="-2.42" width="9.78" height="36.34" transform="translate(37.04 -49.29) rotate(49.27)"/>
                                            <rect class="cls-1" x="102.64" y="-7.32" width="10.99" height="90" transform="translate(1.6 79.69) rotate(-40.73)"/>
                                            <rect class="cls-1" x="58.39" y="48.06" width="35.91" height="10.42"/>
                                            <rect class="cls-1" x="58.39" y="73.56" width="35.91" height="10.42"/>
                                            <rect class="cls-1" x="45.64" y="60.81" width="35.91" height="10.42" transform="translate(129.62 2.42) rotate(90)"/>
                                            <rect class="cls-1" x="71.13" y="60.81" width="35.91" height="10.42" transform="translate(155.11 -23.07) rotate(90)"/>
                                        </g>
                                    </g>
                                </svg>
                            </div>
                            <p>Střešní okna</p>
                        </div>
                    </div>
                    <div class="header-block">
                        <div class="header-block-content">
                            <div class="header-block-image">
        <!--                        <img src="http://localhost:8001/wp-content/uploads/2018/01/vestavby.svg">-->
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 168.65 89.97">
                                    <defs>
                                        <style>.cls-1 {
                                                fill: #fff;
                                            }</style>
                                    </defs>
                                    <title>vestavby</title>
                                    <g id="Vrstva_2" data-name="Vrstva 2">
                                        <g id="vestavby">
                                            <rect class="cls-1" x="37.3" y="0.7" width="35.91" height="10.42"/>
                                            <rect class="cls-1" x="37.3" y="0.7" width="10.99" height="54.98"/>
                                            <rect class="cls-1" x="22.76" y="48.91" width="10.99" height="44.21" transform="translate(61.64 2.26) rotate(47.6)"/>
                                            <rect class="cls-1" x="41.36" y="45.23" width="6.93" height="14.94"/>
                                            <path class="cls-1" d="M73.21.74a0,0,0,0,0,0,0H63.48a0,0,0,0,0,0,0l0,36.11c3.49-3,6.95-6,9.75-8.38Z"/>
                                            <rect class="cls-1" x="75.62" y="-2.42" width="9.78" height="36.34" transform="translate(39.9 -55.53) rotate(49.27)"/>
                                            <polyline class="cls-1" points="154.14 89.97 155.58 89.97 166.36 87.32 91.16 0 82.83 7.17 154.14 89.97"/>
                                            <polygon class="cls-1" points="168.65 89.97 0 89.97 91.08 0.18 124.6 38.83 168.65 89.97"/>
                                        </g>
                                    </g>
                                </svg>
                            </div>
                            <p>Půdní vestavby</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div id="primary" class="content-area">
            <main id="main" class="site-main" role="main">
<!--                <div class="section section-about">-->
<!--                    <div class="wrap">-->
<!--                        <div class="section-content">-->
<!--                            <div class="section-heading">-->
<!--                                <h2>Co děláme</h2>-->
<!--                            </div>-->
<!--                            <p>Firma Ladislav Královský vznikla v roce 1997 a její hlavní činností jsou tesařské,-->
<!--                                pokrývačské a klempířské práce. Naše dlouholeté zkušenosti v oboru Vám zajistí vysokou kvalitu-->
<!--                                provedených prací. Důraz klademe na preciznost a individuální přístup k zákazníkům.</p>-->
<!--                        </div>-->
<!--                    </div>-->

                    <?php if(have_rows( 'block-front-page-content' )):

                    // loop through the rows of data
                    while(have_rows( 'block-front-page-content' )) : the_row(); ?>
                    <div class="section section-<?php the_sub_field( 'front-page-content-class' ) ?>">
                        <div class="wrap">
                            <div class="section-content">
                                <div class="section-heading">
                                    <h2> <?php the_sub_field( 'front-page-content-heading' ) ?> </h2>
                                </div>
                                <?php the_sub_field( 'front-page-content' ) ?>
                            </div>
                        </div>
                    </div>
                    <?php endwhile;
                    else :
                    // no rows found
                    endif; ?>
<!--                </div>-->
        
<!--                <div class="section section-gallery">-->
<!--                    <div class="wrap">-->
<!--                        <div class="section-content">-->
<!--                            <div class="section-heading">-->
<!--                                <h2>Fotogalerie</h2>-->
<!--                                --><?php //echo do_shortcode( "[WRGF id=98]")?>
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
        
               
        
        <!--		--><?php //// Show the selected frontpage content.
        //		if ( have_posts() ) :
        //			while ( have_posts() ) : the_post();
        //				get_template_part( 'template-parts/page/content', 'front-page' );
        //			endwhile;
        //		else : // I'm not sure it's possible to have no posts when this page is shown, but WTH.
        //			get_template_part( 'template-parts/post/content', 'none' );
        //		endif; ?>
        
        <!--		--><?php
        //		// Get each of our panels and show the post data.
        //		if ( 0 !== twentyseventeen_panel_count() || is_customize_preview() ) : // If we have pages to show.
        //
        //			/**
        //			 * Filter number of front page sections in Twenty Seventeen.
        //			 *
        //			 * @since Twenty Seventeen 1.0
        //			 *
        //			 * @param int $num_sections Number of front page sections.
        //			 */
        //			$num_sections = apply_filters( 'twentyseventeen_front_page_sections', 4 );
        //			global $twentyseventeencounter;
        //
        //			// Create a setting and control for each of the sections available in the theme.
        //			for ( $i = 1; $i < ( 1 + $num_sections ); $i++ ) {
        //				$twentyseventeencounter = $i;
        //				twentyseventeen_front_page_section( null, $i );
        //			}
        //
        //	endif; // The if ( 0 !== twentyseventeen_panel_count() ) ends here. ?>
        
            </main><!-- #main -->
        </div><!-- #primary -->

<?php get_footer();
