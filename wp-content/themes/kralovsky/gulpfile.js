var gulp = require('gulp');
var sass = require('gulp-sass');
var cssnano = require('gulp-cssnano');
var rename = require('gulp-rename');
var runSequence = require('run-sequence');
var autoprefixer = require('gulp-autoprefixer');

/**
 * CSS
 **/
gulp.task('sass', function () {
  return gulp.src('assets/scss/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer({browsers: ['last 2 version', 'safari 5', 'ie 9', 'opera 12.1', 'ios 6', 'android 4'], cascade: false}))
    .pipe(gulp.dest(''));
});

/**
 * Minimalization
 */
gulp.task('css-min', function () {
  return gulp.src(['www/css/*.css', '!www/css/**/*.min.css'])
    .pipe(cssnano())
    .pipe(rename({ suffix: '.min' }))
    .pipe(gulp.dest('www/css'));
});

/**
 * Watch
 */
gulp.task('watch', function () {
  gulp.watch('assets/scss/*.scss', ['sass']);
});

gulp.task('css', function(callback) {
  runSequence('sass', 'css-min', callback);
});

/**
 * Default task
 **/

gulp.task('default', function() {
  runSequence(['css'], 'watch');
});

  