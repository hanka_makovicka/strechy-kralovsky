<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */

?>

		</div><!-- #content -->

		<footer id="colophon" class="site-footer" role="contentinfo">
			 <div class="section section-contact">
	            <div class="container-fluid">
	                <div class="row">
	                    <div class="col-md-6 footer-map">
	                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2614.919578973916!2d14.195622816093206!3d49.050152679306755!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4774a97ac9119e8d%3A0xa4510be4764120c9!2sV%C3%A1clavsk%C3%A1+124%2C+384+11+Netolice!5e0!3m2!1scs!2scz!4v1515493821554" width="100%" height="700px" frameborder="0" style="border:0" allowfullscreen></iframe>
	                    </div>
	                    <div class="col-md-6">
	                        <div class="wrap">
	                            <div class="section-content">
	                                <div class="section-heading">
	                                    <h2>Napište nám</h2>
	                                </div>
	                                <?php echo do_shortcode( '[contact-form-7 id="45" title="Contact form"]' ); ?>
	                                <div class="section-heading">
	                                    <h2>Kontakt</h2>
	                                </div>
	                                <table>
	                                    <tr>
                                            <td><span class="icon-block"><i class="fa fa-phone" aria-hidden="true"></i></span><span class="icon-text">Telefon:</span></td>
	                                        <td><a href="tel:<?php the_field( 'phone' ) ?>"><?php the_field( 'phone' ) ?></a></td>
	                                    </tr>
	                                    <tr>
                                            <td><span class="icon-block"><i class="fa fa-envelope-o" aria-hidden="true"></i></span><span class="icon-text">E-mail:</span></td>
	                                        <td><a href="mailto:<?php the_field( 'email' ) ?>"><?php the_field( 'email' ) ?></a> </td>
	                                    </tr>
	                                    <tr>
                                            <td><span class="icon-block"><i class="fa fa-map-marker" aria-hidden="true"></i></span><span class="icon-text">Adresa:</span></td>
	                                        <td><?php the_field( 'address' ) ?></td>
	                                    </tr>
	                                </table>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	            </div>
	        </div>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="rights">
                            <p><span class="icon-block"><i class="fa fa-copyright" aria-hidden="true"></i></span><?php echo date('Y'); ?>, Vsechna prava vyhrazena</p>
                        </div>
                    </div>
                </div>
            </div>
		</footer><!-- #colophon -->
	</div><!-- .site-content-contain -->
</div><!-- #page -->
<?php wp_footer(); ?>

</body>
</html>
