<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="section section-single">
		<div class="section-content">
				<?php twentyseventeen_edit_link( get_the_ID() ); ?>
				<?php
//					the_content();
                    if(have_rows( 'block-page-content' )):
                        
                        // loop through the rows of data
                        while(have_rows( 'block-page-content' )) : the_row(); ?>
                            <div class="wrap">
    
                                <div class="section-content">
                                    <div class="section-heading">
                                        <h2> <?php echo get_sub_field( 'page-content-heading' ) ?> </h2>
                                    </div>
                                    
                                    <p> <?php echo get_sub_field( 'page-content' ) ?></p>
                                </div>
                            </div>
                        <?php endwhile;
                        else :
                        
                        // no rows found
                    
                    endif;
					wp_link_pages( array(
						'before' => '<div class="page-links">' . __( 'Pages:', 'twentyseventeen' ),
						'after'  => '</div>',
					) );
				?>
		</div>
	</div>
</article><!-- #post-## -->
