(function ($) {
    var blocks = $('.header-block-content');

    jQuery.each(blocks, function(i, val){
        // $("#" + val).text("Mine is " + val + ".");
        //
        // // Will stop running after "three"
        // return (val !== "three");

        $(val).hover(function () {
            $(this).find('.submenu-services').toggleClass('show-submenu');
        });
    });

    var path = document.querySelector("svg path");
    var total_length = path.getTotalLength();

    console.log(path);
    console.log(total_length);
})(jQuery);