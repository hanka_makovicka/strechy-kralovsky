<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<!--<div id="masthead" class="site-header single-page">-->
<!--    <div class="wrap">-->
<!--        -->
<!--    </div>-->
<!--</div>-->

    <header id="masthead" class="site-header" role="banner">
        
<!--        --><?php //get_template_part( 'template-parts/header/header', 'image' ); ?>
        <div class="custom-header">
            <div class="custom-header-media">
                <?php the_custom_header_markup(); ?>
<!--                <img src="http://localhost:8001/wp-content/uploads/2018/01/roof-coating-2846324.jpg">-->

            </div>
        </div>
        
        <?php if(has_nav_menu( 'top' )) : ?>
            <div class="navigation-top">
                <div class="wrap">
                    <?php get_template_part( 'template-parts/navigation/navigation', 'top' ); ?>
                </div><!-- .wrap -->
            </div><!-- .navigation-top -->
        <?php endif; ?>


    </header><!-- #masthead -->

<?php

/*
 * If a regular post or page, and not the front page, show the featured image.
 * Using get_queried_object_id() here since the $post global may not be set before a call to the_post().
 */
if(( is_single() || ( is_page() && !twentyseventeen_is_frontpage() ) ) && has_post_thumbnail( get_queried_object_id() )) :
    echo '<div class="single-featured-image-header">';
    echo get_the_post_thumbnail( get_queried_object_id(), 'twentyseventeen-featured-image' );
    echo '</div><!-- .single-featured-image-header -->';
endif;
?>

<div class="site-content-contain">
    <div id="content" class="site-content">
        <div class="wrap">
            <div id="primary" class="content-area">
                <main id="main" class="site-main" role="main">
                    <div class="header-blocks">
                        <div class="header-block">
                            <div class="header-block-content">
                                <div class="header-block-image">
        
                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 141.66 89.97">
                                        <defs>
                                            <style>.cls-1 {
                                                    fill: #fff;
                                                }</style>
                                        </defs>
                                        <title>strechy</title>
                                        <g id="strechy" data-name="Vrstva 2">
                                            <g id="strechy">
                                                <rect class="cls-1" x="29.07" y="0.7" width="35.91" height="10.42"/>
                                                <rect class="cls-1" x="29.07" y="0.7" width="10.99" height="54.98"/>
                                                <rect class="cls-1" x="14.53" y="48.91" width="10.99" height="44.21" transform="translate(58.96 8.34) rotate(47.6)"/>
                                                <rect class="cls-1" x="33.13" y="45.23" width="6.93" height="14.94"/>
                                                <path class="cls-1" d="M65,.74a0,0,0,0,0,0,0h-9.7a0,0,0,0,0,0,0l0,36.11c3.49-3,7-6,9.75-8.38Z"/>
                                                <rect class="cls-1" x="67.38" y="-2.42" width="9.78" height="36.34" transform="translate(37.04 -49.29) rotate(49.27)"/>
                                                <rect class="cls-1" x="102.64" y="-7.32" width="10.99" height="90" transform="translate(1.6 79.69) rotate(-40.73)"/>
                                            </g>
                                        </g>
                                    </svg>
                                </div>
                                <?php the_title( '<p>', '</p>' ); ?>
                            </div>
                        </div>
                    </div>
        
                    <?php
                    while ( have_posts() ) : the_post();
        
                        get_template_part( 'template-parts/page/content', 'page' );
        
                        // If comments are open or we have at least one comment, load up the comment template.
                        if ( comments_open() || get_comments_number() ) :
                            comments_template();
                        endif;
        
                    endwhile; // End of the loop.
                    ?>
        
                </main><!-- #main -->
            </div><!-- #primary -->
        </div><!-- .wrap -->

<?php get_footer();
