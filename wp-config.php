<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'strechykralovsky');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'mysql');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '+T@Ad[YZ752[%q/X2;LO#3qKFlz~=.q*2W=%FhO@_a?yRbFGJVSG|f;iM>kR##Pw');
define('SECURE_AUTH_KEY',  '%}p8{,ds#p8<1ajW }}Mzb?_R,<Axa`HNm9zs<8?:(gO&>Nd|-;T_2#0yv+R>3,V');
define('LOGGED_IN_KEY',    'isoD;bkw%--4G_~9*}r-K;[ab]!6Pi7rd i0uu,w[Nt^@`kmvRol4wQ=L.Z=716<');
define('NONCE_KEY',        'H-mk=4VTF[Ply!.h{;oOV+uh0I>p8|r^EH~#`Pg1S]9D;lL?EKhyrf[#LtB`EhtA');
define('AUTH_SALT',        '2QS9)Ir:/Z6Ii^!0}Xu)!Z{gElt<;%Weonf)_sdARB?EvDbr0~Zo]xOc~5q8#R(z');
define('SECURE_AUTH_SALT', 'n2)lo!# o[TuL$j5-&IWs)pt1bFa;3E>4x?6Rrd_xl_~_Nin{pMewmOu@~Vdn/_f');
define('LOGGED_IN_SALT',   '=k<y*6W<.O>M4n:+n<lqg5_}MIVv)nbL_RW/]mQRT=sA[ )_0/TdcR5ezO(=nX@?');
define('NONCE_SALT',       '%Z`LVvu0h{[uY/%?t!]fWUy4D/z4TWAgbyHyk;H9a8C1:pX0tz+94 &+ Bpy1V4b');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plusgin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
